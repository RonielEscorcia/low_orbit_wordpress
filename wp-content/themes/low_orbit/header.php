<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="container alcien">
            <div class="row">
                <div class="col-xs-3 col-sm-2">
                    <img id="logo" src="<?php bloginfo('template_directory'); ?>/dist/img/logo.png">
                </div>

                <div class="col-xs-9 col-sm-10">
                    <nav>
                        <div class="suggestion"><input type="text" placeholder="Make a suggestion"/></div>
                        <div class="search"><input type="search" placeholder="Search"/></div>
                    </nav>
                    
                </div>
                
            </div>
        </div>
        
    </header>