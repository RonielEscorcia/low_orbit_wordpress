<?php get_header(); ?>

        <div id="main-content">
            <div class="container alcien">
            
                <div class="row">                    
					<?php 
					if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <article>
                            <div class="row">
                                <div class="col-xs-11">
                                    <h1><?php the_title(); ?></h1>
                                </div>
                                <div class="col-xs-1">
                                    <a href="#" class="expand" data-id="<?php echo $post->ID ?>"><span class="icon icon-flechas-expandir"></span></a>
                                </div>
                            </div>    

                            <div class="row">
                                <div class="col-xs-12">
                                	<?php $slogan = get_post_meta($post->ID, "slogan", $single = true); ?>                                    
                                    <h2><?php echo $slogan ?></h2>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <?php $category = get_post_meta($post->ID, "category", $single = true); ?>
                                    <hr class="<?php echo $category ?>">
                                   	<?php //Verifica si el post tiene una imágen destacada
										if ( has_post_thumbnail() ) {
 
										//Si tiene imágen destacada entonces carga la imagen en tamaño thumbnail (miniatura), y le añade la clase alignleft
										the_post_thumbnail();
									}
									?>

									<?php $short_description = get_post_meta($post->ID, "short_description", $single = true); ?>
                                    <p><?php echo $short_description ?></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-8">
                                	<?php $status = get_post_meta($post->ID, "status", $single = true); ?>
                                    <p class="info"><span class="icon icon-info"></span> <?php echo $status?></p>
                                    <?php $link = get_post_meta($post->ID, "link", $single = true); ?>
                                    <a href="<?php echo $link ?>"><span class="icon icon-link"></span> <?php  echo $link?></a>
                                </div>

                                <div class="col-xs-4">
                                    <button><span class="icon icon-share"></span> Share</button>
                                </div>
                            </div>
                        </article>
                    </div>

                	<?php 
                		endwhile;
                	else: //Ahora bien, si no hay artículos para mostrar entonces cargará lo siguiente  ?>
 
						<h2>We do not have items available</h2>
 
					<?php endif; // Aquí termina el loop?>
                </div>
            </div>            
        </div>

        <div id="expand">
            
        </div>

<?php get_footer(); ?>