var imagenActual = 0;
var imagenesTotal;
var imagenes;

var populateExpandField = function(obj) {
	
	var plantilla = MyApp.templates.expand2(obj);
	$('#expand').html(plantilla);

	$('#expand').addClass('show'); 

	$('a.cerrar').click(function(e){
    	$('#expand').removeClass('show');
    	$('#expand hr').removeClass();
    	
    	e.preventDefault();
    });
  }
var getPost = function(postId) {
	return $.getJSON('wp-admin/admin-ajax.php', {action: 'detailPost', 'id': postId}, function(data) {
			console.log(data);
  			populateExpandField(data);
	});	
}

var cambiarImagen = function (dir) {
	// calcula el numero de imagenes en el slider
	var imagen = imagenes[imagenActual];
	var bullets = $('#bullets ul li');
 	$(imagen).removeClass('left');

	if(dir == 'siguiente' && (imagenActual >= (imagenesTotal-1 ))){
		
		imagenActual = 0;
		//imagenes.removeClass('left');
		var margen = -100 * (imagenActual);
		$('#slider').css("margin-left", margen + "%");

	}else if(dir == 'siguiente'){
		
		//$(imagen).addClass('left');
		imagenActual++;	
		var margen = -100 * (imagenActual);
		$('#slider').css("margin-left", margen + "%");

	}else if(dir == 'previo' && (imagenActual <= 0)){
		imagenActual = (imagenesTotal - 1);
		var margen = -100 * (imagenActual);
		$('#slider').css("margin-left", margen + "%");
		//$('#slider > li:not(:last-child)').addClass('left');

	}else{
		
		$(imagenes[imagenActual-1]).removeClass('left');
	 	imagenActual--;	 	
	}

	bullets.removeClass("active");
	$(bullets[imagenActual]).addClass('active');

}

$(document).ready(function() {
	imagenes = $('#slider li');
	imagenesTotal = $('#slider li').length;

    $('a.expand').on('click', function(e) {
    	var id = $(this).data('id');
    	getPost(id);
    	e.preventDefault();
    });

    
    $('.controls').on('click', function (e) {
    	// esta es la direccion si es siguiente o previo
    	var dir = $(this).attr('id');
    	cambiarImagen(dir);
    	e.preventDefault();
    });

    $('#bullets ul li').click(function(){
    	var id = $(this).data('ref');
    	var sliders = $('#slider li');

    	$('#slider li').removeClass("left");
    	$('#bullets ul li').removeClass("active");

    	for(i = 0; i < id; i++){
    		
    		$(sliders[i]).addClass('left');
    	}
    	imagenActual = id;
    	
    	$(this).addClass('active');
    });
});