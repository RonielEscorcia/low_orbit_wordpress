<?php 
	function detailPost(){
		$id = $_GET['id'];
		$post = get_post($id);
		$post_custom_field = get_post_custom_keys($id);
		$status = get_post_meta($id, "status", $single = true);
		$price = get_post_meta($id, "price", $single = true);
		$price = number_format($price, 2);
		if ($status == "Still in orbit") {
			$status = "Landed";
		}

		$result = array(
			'title' => $post->post_title,
			'content' => $post->post_content,
			'slogan' => get_post_meta($id, "slogan", $single = true),
			'status' => $status,
			'price' => $price,
			'link' => get_post_meta($id, "link", $single = true),
			'category' => get_post_meta($id, "category", $single = true),

		);



		echo json_encode($result);
		die();
	}

	add_action('wp_ajax_detailPost', 'detailPost');
	add_action('wp_ajax_nopriv_detailPost', 'detailPost');
?>