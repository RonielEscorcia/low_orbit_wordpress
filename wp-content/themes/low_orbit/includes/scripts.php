<?php 
	//Cargando el css de nuestro tema.
	function themeStyles(){
		wp_enqueue_style('bootstrap_css', get_stylesheet_directory_uri()."/dist/css/bootstrap.min.css");
		wp_enqueue_style('main_css', get_stylesheet_directory_uri()."/dist/css/main.css");
	}

	add_action( 'wp_enqueue_scripts', 'themeStyles');


	//Cargamos los scripts de nuestro tema
	function themeScripts() {
 		
		
		wp_register_script( 'bootstrap_js', get_stylesheet_directory_uri().'/dist/js/bootstrap.min.js', array('jquery'), '', true );
	 
		wp_register_script( 'handlebars_js', get_stylesheet_directory_uri().'/dist/js/handlebars-v4.0.5.js', array('jquery'), '', true );


		wp_register_script( 'template_js', get_stylesheet_directory_uri().'/dist/js/templates.js', array('handlebars_js'), '', true );

		wp_register_script( 'main_js', get_stylesheet_directory_uri().'/dist/js/main.js', array('template_js'), '', true );

		wp_enqueue_script( 'main_js');


	}
 
	add_action( 'wp_enqueue_scripts', 'themeScripts');
?>