<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_low_orbit');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0V`jI<XX 5/Ve>MaAc`lsEU+(Y8RZ~$qBRh|Ai/JgW@+5-H;hXYg;0]qy%P;1Y*s');
define('SECURE_AUTH_KEY',  'q]-#fjqeF4vR,Vtd9X,YY)rxgMVTfor8Z(D8({soD41FAioK-=W{RLLReVSoK|es');
define('LOGGED_IN_KEY',    'kuxNlv+=u^m(gG#:%X]X,fcDH1irMwdvsnQqha?l{zQE?7(R+.R:nlF;wrF*Qo4K');
define('NONCE_KEY',        ':R3N5f(Q?1ukE$0H<Af%Zqe_<dD},}EgPWX%`NsM%&u+]&A&a6ewqa>/*( J|r^i');
define('AUTH_SALT',        'firaSK2a&g1@gBvdXbor.n|_CqpT.GYt_(2$^nCA+[tDCrZ*6S9`jX!dv nemOd;');
define('SECURE_AUTH_SALT', '/htH)xTRsx?#+pg%sQG~l*>Ky(Q9A9V~+8m9j8jcJo_,;BZ=`-{yZ_:(K][LRUpE');
define('LOGGED_IN_SALT',   'u<u%hq4+[t+t-?R2dGj)g$n&Y?eQ`9[-/;+Q~&mul^@vrg[%XA<]}}V}S!L)-fY&');
define('NONCE_SALT',       'S:juUlE@|_cCNf[Zv)j~~2LuX$o7zSnx0r PA?@.sB5Ez[]Mv1JT6s{w%/(&&&:U');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
